﻿using System;
namespace Online.Interface.Model
{
    public class Profile
    {
        public Profile()
        {
        }

        public int Key { get; set; }
        public string UserKey { get; set; }

        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }

        public string Country { get; set; }
        public string Language { get; set; }
        public string EthnicOrigin { get; set; }
        public string Qualification { get; set; }
        public string Employment { get; set; }
        public string Industry { get; set; }
        public string Position { get; set; }
        public string Research { get; set; }
    }
}