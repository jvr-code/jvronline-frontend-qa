﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Online.Interface.Data.Custom
{
    public partial class Profile
    {
        public int Key { get; set; }
        public string UserKey { get; set; }
        public string Gender { get; set; }
        public string BirthDate { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string EthnicOrigin { get; set; }
        public string Qualification { get; set; }
        public string Employment { get; set; }
        public string Industry { get; set; }
        public string Position { get; set; }
        public string Research { get; set; }
    }
}
