﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Online.Interface.Data.Custom;

namespace Online.Interface.Pages
{
    public class VerificationModel : PageModel
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private JvrCustomContext _database { get; set; }
        public List<Country> _countries { get; set; }

        public VerificationModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<IActionResult> OnGet(string Id)
        {
            IdentityUser user = await userManager.FindByIdAsync(Id);
            if (user == null)
            {
                return RedirectToPage("Login");
            }
            else
            {
                user.EmailConfirmed = true;
                await userManager.UpdateAsync(user);
                return Page();
            }
        }
    }
}