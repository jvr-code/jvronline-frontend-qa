﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Online.Interface.Data.Custom;
using Online.Interface.Data.User;

namespace Online.Interface.Pages
{
    [Authorize]
    public class ProfileModel : PageModel
    {
        public Model.Profile ModelProfile { get; set; }

        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private JvrCustomContext database { get; set; }
        private JVRContext customUser { get; set; }

        public ProfileModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public void OnGet()
        {
            database = new JvrCustomContext();
            customUser = new JVRContext();

            string UserKey = userManager.GetUserId(User);

            Data.Custom.Profile profile = database.Profile.Where(a => a.UserKey == UserKey).FirstOrDefault();
            AspNetUsers aspNetUsers = customUser.AspNetUsers.Where(a => a.Id == UserKey).FirstOrDefault();

            if (profile != null && aspNetUsers != null)
            {
                string[] names = aspNetUsers.UserName.Split('.');
                ModelProfile = new Model.Profile
                {
                    FirstName = names[0],
                    Surname = names[1],
                    Gender = profile.Gender,
                    BirthDate = profile.BirthDate,
                    Email = aspNetUsers.Email,
                    PasswordHash = aspNetUsers.PasswordHash,
                    PhoneNumber = aspNetUsers.PhoneNumber,
                    Country = profile.Country,
                    Language = profile.Language,
                    EthnicOrigin = profile.EthnicOrigin,
                    Qualification = profile.Qualification,
                    Position = profile.Position,
                    Employment = profile.Employment,
                    Industry = profile.Industry,
                    Research = profile.Research
                };
            }
            else
            {
                ModelProfile = new Model.Profile();
            }
        }
    }
}
