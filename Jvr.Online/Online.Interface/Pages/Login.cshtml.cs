﻿using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Online.Interface.Pages
{
    public class LoginModel : PageModel
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;

        public LoginModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {

            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public void OnGet()
        {
            if (User.Identity.IsAuthenticated)
                signInManager.SignOutAsync();
        }

        public async Task<IActionResult> OnPost()
        {
            IdentityUser user = await userManager.FindByEmailAsync(Request.Form["emailaddress"]);
            Microsoft.AspNetCore.Identity.SignInResult result = await signInManager.PasswordSignInAsync(user, Request.Form["password"], true, false);

            if (result.Succeeded)
            {
                await userManager.AddClaimAsync(user, new Claim("UserRole", "Admin"));
                return RedirectToPage("Index");
            }
            else
            {
                return Page();
            }
        }

        public async Task<IActionResult> OnPostPassword(string emailAddress)
        {
            IdentityUser user = await userManager.FindByEmailAsync(emailAddress);
            if (user == null)
            {
                return new JsonResult(new { code = "1" });
            }
            else
            {
                MailMessage mail = new MailMessage();

                MailAddress talent = new MailAddress(emailAddress);
                MailAddress from = new MailAddress("no-reply@bluemonday.co.za");

                mail.From = from;
                mail.Bcc.Add(talent);

                mail.Subject = "JVR Online – Password reset";

                string body;
                using (StreamReader reader = new StreamReader("wwwroot/tmp/Password.html"))
                {
                    body = reader.ReadToEnd();
                }

                body =
                    body.Replace("{Name}", user.UserName.Replace(".", " "))
                    .Replace("{link}", string.Format("http://jvr.mentrix.co.za/Reset?Id={0}", user.Id));

                mail.Body = body;
                mail.IsBodyHtml = true;

                SmtpClient client = new SmtpClient()
                {
                    Host = "smtp.office365.com",
                    Port = 587,
                    EnableSsl = true,
                    Credentials = new NetworkCredential("rmahomed@bluemonday.co.za", "$BlueRM2020")
                };

                client.Send(mail);
                return new JsonResult(new { code = "0" });
            }
        }
    }
}
