﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Online.Interface.Pages
{
    public class SupportModel : PageModel
    {
        public void OnGet()
        {
        }

        public IActionResult OnPost(string emailAddress, string fullName)
        {
            MailMessage mail = new MailMessage();

            MailAddress talent = new MailAddress(emailAddress);
            MailAddress from = new MailAddress("no-reply@bluemonday.co.za");

            mail.From = from;
            mail.Bcc.Add(talent);

            mail.Subject = "JVR Online – Confirmation of support request";

            string body;
            using (StreamReader reader = new StreamReader("wwwroot/tmp/Support.html"))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Name}", fullName);

            mail.Body = body;
            mail.IsBodyHtml = true;

            SmtpClient client = new SmtpClient()
            {
                Host = "smtp.office365.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("rmahomed@bluemonday.co.za", "$BlueRM2020")
            };

            client.Send(mail);
            return new JsonResult(new { code = "0" });
        }
    }
}