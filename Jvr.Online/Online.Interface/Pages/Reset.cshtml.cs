﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Online.Interface.Pages
{
    public class ResetModel : PageModel
    {
        private string userId { get; set; }
        private readonly UserManager<IdentityUser> userManager;

        public ResetModel(UserManager<IdentityUser> userManager)
        {

            this.userManager = userManager;
        }

        public void OnGet(string Id)
        {
            ViewData["Tokan"] = Id;
        }

        public async Task<IActionResult> OnPostReset(string password, string tokan)
        {
            IdentityUser user = await userManager.FindByIdAsync(tokan);
            string token = await userManager.GeneratePasswordResetTokenAsync(user);
            IdentityResult result = await userManager.ResetPasswordAsync(user, token, password);

            if (result.Succeeded)
                return new JsonResult(new { code = "0" });
            else
                return new JsonResult(new { code = "1" });
        }
    }
}
