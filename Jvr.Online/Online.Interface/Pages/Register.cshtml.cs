﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Online.Interface.Data.Custom;

namespace Online.Interface.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private JvrCustomContext _database { get; set; }
        public List<Country> _countries { get; set; }

        public RegisterModel(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostRegisterUser(
            string firstName,
            string surname,
            string emailAddress,
            string password,
            string mobileNumber,

            string gender,
            string birthDate,
            string country,
            string language,
            string ethnicOrigin,
            string qualification,

            string employment,
            string industry,
            string position,
            bool research)
        {
            IdentityUser userCheck = await userManager.FindByEmailAsync(emailAddress);
            if (userCheck == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = string.Format("{0}.{1}", firstName, surname),
                    NormalizedUserName = emailAddress,
                    Email = emailAddress,
                    PhoneNumber = mobileNumber,
                    EmailConfirmed = false,
                    PhoneNumberConfirmed = false,
                    LockoutEnabled = false
                };
                IdentityResult result = await userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    user = await userManager.FindByEmailAsync(emailAddress);
                    Profile profile = new Profile
                    {
                        Gender = gender,
                        BirthDate = birthDate,
                        Country = country,
                        Language = language,
                        EthnicOrigin = ethnicOrigin,
                        Qualification = qualification,
                        Employment = employment,
                        Industry = industry,
                        Position = position,
                        Research = research.ToString(),
                        UserKey = user.Id
                    };
                    _database = new JvrCustomContext();
                    await _database.Profile.AddAsync(profile);
                    await _database.SaveChangesAsync();

                    MailMessage mail = new MailMessage();

                    MailAddress talent = new MailAddress(emailAddress);
                    MailAddress from = new MailAddress("no-reply@bluemonday.co.za");

                    mail.From = from;
                    mail.Bcc.Add(talent);

                    mail.Subject = "JVR Online - Verify your email address";

                    string body;
                    using (StreamReader reader = new StreamReader("wwwroot/tmp/Verification.html"))
                    {
                        body = reader.ReadToEnd();
                    }

                    body =
                        body.Replace("{Name}", firstName)
                        .Replace("{Surname}", surname)
                        .Replace("{link}", string.Format("http://jvr.mentrix.co.za/Verification?Id={0}", user.Id));

                    mail.Body = body;
                    mail.IsBodyHtml = true;

                    SmtpClient client = new SmtpClient()
                    {
                        Host = "smtp.office365.com",
                        Port = 587,
                        EnableSsl = true,
                        Credentials = new NetworkCredential("rmahomed@bluemonday.co.za", "$BlueRM2020")
                    };

                    client.Send(mail);

                    return new JsonResult(new { code = "0"});
                }
                else
                {
                    return new JsonResult(new { code = "1" });
                }
            }
            else
            {
                return new JsonResult(new { code = "2" });
            }
        }
    }
}