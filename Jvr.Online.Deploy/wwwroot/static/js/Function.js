﻿$(function () {
    $('#complete').click(function () {
        var result;

        if ($('#yesCheck').is(':checked')) {
            result = 'true';
        } else {
            result = 'false';
        }

        $.ajax({
            type: 'POST',
            url: '/Register?handler=RegisterUser',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('XSRF-TOKEN',
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: {
                'firstName': $('#firstName').val(),
                'surname': $('#surname').val(),
                'emailAddress': $('#emailAddress').val(),
                'password': $('#password').val(),
                'mobileNumber': $('#mobileNumber').val(),

                'gender': $('#gender').val(),
                'birthDate': $('#birthDate').val(),
                'country': $('#country').val(),
                'language': $('#language').val(),
                'ethnicOrigin': $('#ethnicOrigin').val(),
                'qualification': $('#qualification').val(),

                'employment': $('#employment').val(),
                'industry': $('#industry').val(),
                'position': $('#position').val(),
                'research': result
            },
            success: function (x) {
                if (x.code == "0")
                    window.location.href = "/Login";
                else
                    $('#alertBanner').append('<div class="alert" role="alert" style="background-color: #F30B33; text-align: center; color: white;"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;<b>Error</b>: could not connect to the server.Please try again later or get in touch with <b>support</b>.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white;"><span aria-hidden="true">&times;</span></button></div>');
            }
        });
    });
});

$(function () {
    $('#passwordReset').click(function () {
        $.ajax({
            type: 'POST',
            url: '/Login?handler=Password',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('XSRF-TOKEN',
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: {
                'emailAddress': $('#email').val()
            },
            success: function (x) {
                if (x.code == "0")
                    $("#carousel").carousel("next");
                else
                    $('#alertBanner').append('<div class="alert" role="alert" style="background-color: #F30B33; text-align: center; color: white;"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;<b>Error</b>: could not connect to the server.Please try again later or get in touch with <b>support</b>.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white;"><span aria-hidden="true">&times;</span></button></div>');
            }
        });
    });
});

$(function () {
    $('#resetForm').on('submit', function (e) {
        e.preventDefault();
        if ($('#passwordReset').val() == $('#ConfirmPasswordReset').val()) {
            $.ajax({
                type: 'POST',
                url: '/Reset?handler=Reset',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('XSRF-TOKEN',
                        $('input:hidden[name="__RequestVerificationToken"]').val());
                },
                data: {
                    'password': $('#passwordReset').val(),
                    'tokan': $('#tokan').val()
                },
                success: function (x) {
                    if (x.code == "0")
                        $("#carousel").carousel("next");
                    else
                        $('#alertBanner').append('<div class="alert" role="alert" style="background-color: #F30B33; text-align: center; color: white;"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;<b>Error</b>: could not connect to the server.Please try again later or get in touch with <b>support</b>.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white;"><span aria-hidden="true">&times;</span></button></div>');
                }
            });
        }
        else {
            $('#alertBanner').append('<div class="alert" role="alert" style="background-color: #F30B33; text-align: center; color: white;"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;<b>Error</b>: could not connect to the server.Please try again later or get in touch with <b>support</b>.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white;"><span aria-hidden="true">&times;</span></button></div>');
        }
    });
});

$(function () {
    $('#supportForm').on('submit', function (e) {
        e.preventDefault();
        if ($("#supportForm").valid()) {
            $.ajax({
                type: 'POST',
                url: '/Support',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('XSRF-TOKEN',
                        $('input:hidden[name="__RequestVerificationToken"]').val());
                },
                data: {
                    'emailAddress': $('#emailAddress').val(),
                    'fullName': $('#fullName').val()
                },
                success: function (x) {
                    if (x.code == "0") {
                        $("#carousel").carousel("next");
                        topFunction();
                    }
                    else
                        $('#alertBanner').append('<div class="alert" role="alert" style="background-color: #F30B33; text-align: center; color: white;"><i class="fa fa-exclamation-triangle"></i>&nbsp;&nbsp;<b>Error</b>: could not connect to the server.Please try again later or get in touch with <b>support</b>.<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: white;"><span aria-hidden="true">&times;</span></button></div>');
                }
            });
        }
    });
});